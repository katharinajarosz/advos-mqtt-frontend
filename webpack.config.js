/* eslint no-undef: "error"*/
/* eslint-env node*/
const path = require('path');
const webpack = require('webpack');

module.exports = {
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.mp4']
  },
  entry: [
    'webpack/hot/only-dev-server',
    'react-hot-loader/patch',
    path.resolve(__dirname, './src/app/index.js')
  ],
  output: {
    path: path.resolve(__dirname, '/public/'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'eslint-loader',
        options: {
          formatter: require('eslint/lib/formatters/html'),
          fix: true,
          configFile: path.resolve(__dirname, '.eslintrc.json')
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['env', 'react'],
          plugins: ['syntax-jsx', 'transform-react-jsx',
            'transform-react-display-name']
        }
      },
      {
        test: /\.css$/,
        use: [
          {loader: 'style-loader'},
          {loader: 'css-loader'}
        ]
      },
      {test: /\.(png|jpg)$/, loader: 'url-loader?limit=100000'},

      {test: /\.json$/, loader: 'json-loader'},
      {
        test: /\.(jpg)$/,
        loader: 'file-loader'
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.html$/,
        loader: 'html-loader?attrs[]=video:src'
      }, {
        test: /\.mp4$/,
        loader: 'url-loader?limit=10000&mimetype=video/mp4'
      }

    ]
  },
  plugins: [
    // new webpack.optimize.UglifyJsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),
    new webpack.HotModuleReplacementPlugin()
    // new CleanWebpackPlugin(['src/copy/*-compiled.*'])
  ],
  devServer: {

    contentBase: path.join(__dirname, '/public/'),
    compress: true,
    port: 9000,
    hot: true,
    progress: true
  }
};
