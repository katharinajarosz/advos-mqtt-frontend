To start the example application
1. Clone the repo
2. Install yarn
3. You also need a mqtt message broker.
   You can install one on your local machine.
   I use EMQ. It is available on http://emqtt.io/.
   There are good instructions for starting and installing the message broker on the website.
4. Next go into top level folder of the cloned project and type into an comandline interface: yarn start.
5. If you open the browser on http://localhost:9000/ no content should be shown.
6. To see anything you have to send a message with content 'behandlung' or 'vorbereitung' to the topic '/state'.
7. The EMQ broker contains a dashboard with an mqtt client. You can access it via http://localhost:18083.
The initial account is 'admin' with password 'public'. On 'Tools' there is a item 'Websocket' where you can connect, pulish and subcribe to the broker.


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Title - ADVOS Frontend

# Installation
To get the application running, you need several tools as explained below

## Developed with
* **Webstorm** - An IDE which includes support for debugging, embedded Git control, syntax highlighting, intelligent code completion and code refactoring.
* **Node.js** - Javascript runtime
* **React** - A javascript library for building user interfaces
* **Babel** - A transpiler for javascript
* **Webpack** - A module bundler for modern JavaScript applications. When webpack processes your application, it recursively builds a dependency graph that includes every module your application needs,
            then packages all of those modules into a small number of bundles - often only one - to be loaded by the browser
* **Bootstrap** - Bootstrap is an open source toolkit for developing with HTML, CSS, and JS
* **ESLint** - The pluggable linting utility for JavaScript and JSX
* **Yarn** - Yarn is a new package manager for Javascript
* **EMQ** - EMQ(Erlang MQTT Broker) is a distributed, massively scalable, highly extensible MQTT message broker

# Getting Started
* These instructions will get you a copy of the project up and running on your local machine for development

## Prerequisites
* Node js - Download the latest version of the software and install it on your system
* Yarn - After installation of Node js, go to the website `https://yarnpkg.com` and follow the installation instructions
* Webstorm IDE - Install Webstorm from the website` https://www.jetbrains.com/webstorm/`
* EMQ Broker - Download the EMQ Mqtt broker in your system from the website ` http://emqtt.io/`
               Unzip it and start the broker(Can be done while Running the application)

## Project Set Up
### General Setup
* SourceTree: Clone the project 'mqtt-frontend' from the repository into defined workspace folder.

        
   
            | Content           |                 Description                             |
            |-------------------|---------------------------------------------------------|
		| URL               | https://bitbucket.hepawash.com/scm/ad/mqtt-frontend.git |
		| USER              |                                                         |
		| Destination Path  | *local destination for this repository*                 |
		| Name              | *the name of the local repository*                      |

###Setup WebStorm
* Open Webstorm
* From the main menu click ` Open`
* Choose the workspace you checked out the repository into.
* Run `yarn install` like proposed by your IDE, to install the node modules. (you can do that via CLI, too)
 -> this may take while


## Running the Application
* Go into top level folder of the cloned project, right click and select `Open in Terminal`
* A terminal opens up (you are in the top level folder of the project)
* Now type in the command line interface: `yarn start`
* If you open the browser on `http://localhost:9000/` no content should be shown.
* To see anything you have to send a message with content 'Mainmenu' or 'vorbereitung' to the topic '/state'.
* Open CLI and got to emqttd folder by typing `cd emqttd`
* Now start the EMQ Broker by typing `./bin/emqttd start`
* The EMQ broker contains a web dashboard with a mqtt client. A Web Dashboard will be loaded when the EMQ broker is started successfully.
  You can access it via `http://localhost:18083`.
 * The initial account is
    + Username: `admin`
    + password: ` public`
 * On the left side of the dashboard, you can see `Tools` where there is an item `Websocket`, click on it.
 * Now you can see the page, where you can connect, publish and subscribe to the broker.
    + In the connect - Click on `connect` button.Now the broker is connected
    + In the subscribe - Topic: `/state` or the topic which is to be subscribed
    + In Messages - Topic: `/state`
                    Messages: `Mainmenu`
    + Now Enter or click on `send` on the right side.
 * The published messages can be seen in the dashboard below Messages column
 * You should be able to see the project running by now

## Development
* Follow the approved UX-Prototype to develop the application
* The various components of the application is under the folder **src\components**
* These components are assembled into different pages under the folder **src\app**
* The transition from one page to another(one state to another state) is done by using statemachine -**src\fsm**
* The states of the Application is assembled in the index file present in **src\app**
* Other than different states, the application also contains a header and a static footer which also consists of different components.
* A Component is made up of two sub components:
    + PureComponent _(Eg: PureButton)_ - Pure Components are primarily concerned with how things look
    + MainComponent _(Eg: Button)_ - Main Components are primarily concerned with how things work
* Videos and images are stored in assets folder
* Developer tools can be used in the browser to get a detailed picture of the page
* Press `Ctrl+Shift+I` in the browser to get the developer tools window
* A React developer plugin can also be installed in the browser which is helpful in getting the clear picture of react components with props
	
## Committing
Please make sure to only commit the source code. Do not commit, e.g. node_modules or build folders.










