import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
/* global document */

/**
 *
 */
class ProductCategoryRow extends React.Component {
  /**
   *
   * @return {*}
   */
  render() {
    const category = this.props.category;
    return (
      <tr>
        <th colSpan="2">
          {category}
        </th>
      </tr>
    );
  }
}
ProductCategoryRow.propTypes = {
  category: PropTypes.string
};

/**
 *
 */
class ProductRow extends React.Component {
  /**
   *
   * @return {*}
   */
  render() {
    const product = this.props.product;
    const name = product.stocked ?
      product.name :
      <span style={{color: 'red'}}>
        {product.name}
      </span>;

    return (
      <tr>
        <td>{name}</td>
        <td>{product.price}</td>
      </tr>
    );
  }
}

ProductRow.propTypes = {
  product: PropTypes.object
};

/**
 *
 */
class ProductTable extends React.Component {
  /**
   *
   * @return {*}
   */
  render() {
    const filterText = this.props.filterText;
    const inStockOnly = this.props.inStockOnly;

    const rows = [];
    let lastCategory = null;

    this.props.products.forEach((product) => {
      if (product.name.indexOf(filterText) === -1) {
        return;
      }
      if (inStockOnly && !product.stocked) {
        return;
      }
      if (product.category !== lastCategory) {
        rows.push(
            <ProductCategoryRow
              category={product.category}
              key={product.category} />
        );
      }
      rows.push(
          <ProductRow
            product={product}
            key={product.name}
          />
      );
      lastCategory = product.category;
    });

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

ProductTable.propTypes = {
  filterText: PropTypes.object,
  inStockOnly: PropTypes.object,
  products: PropTypes.object
};

/**
 *
 */
class SearchBar extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockChange = this.handleInStockChange.bind(this);
  }

  /**
   *
   * @param {Object} e
   */
  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  /**
   *
   * @param {Object} e
   */
  handleInStockChange(e) {
    this.props.onInStockChange(e.target.checked);
  }

  /**
   *
   * @return {*}
   */
  render() {
    return (
      <form>
        <input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
        <p>
          <input
            type="checkbox"
            checked={this.props.inStockOnly}
            onChange={this.handleInStockChange}
          />
          {' '}
          Only show products in stock
        </p>
      </form>
    );
  }
}

SearchBar.propTypes = {
  onFilterTextChange: PropTypes.object,
  onInStockChange: PropTypes.object,
  filterText: PropTypes.object,
  inStockOnly: PropTypes.object
};

/**
 *
 */
class FilterableProductTable extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      inStockOnly: false
    };

    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockChange = this.handleInStockChange.bind(this);
  }

  /**
   *
   * @param {Object} filterText
   */
  handleFilterTextChange(filterText) {
    this.setState({
      filterText: filterText
    });
  }

  /**
   *
   * @param {Object} inStockOnly
   */
  handleInStockChange(inStockOnly) {
    this.setState({
      inStockOnly: inStockOnly
    });
  }

  /**
   *
   * @return {*}
   */
  render() {
    return (
      <div>
        <SearchBar
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
          onFilterTextChange={this.handleFilterTextChange}
          onInStockChange={this.handleInStockChange}
        />
        <ProductTable
          products={this.props.products}
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
        />
      </div>
    );
  }
}

FilterableProductTable.propTypes = {
  products: PropTypes.object
};


const PRODUCTS = [
  {category: 'Sporting Goods',
    price: '$49.99',
    stocked: true,
    name: 'Football'},
  {category: 'Sporting Goods',
    price: '$9.99',
    stocked: true,
    name: 'Baseball'},
  {category: 'Sporting Goods',
    price: '$29.99',
    stocked: false,
    name: 'Basketball'},
  {category: 'Electronics',
    price: '$99.99',
    stocked: true,
    name: 'iPod Touch'},
  {category: 'Electronics',
    price: '$399.99',
    stocked: false,
    name: 'iPhone 5'},
  {category: 'Electronics',
    price: '$199.99',
    stocked: true,
    name: 'Nexus 7'}
];

ReactDOM.render(
    <FilterableProductTable products={PRODUCTS} />,
    document.getElementById('container')
);
