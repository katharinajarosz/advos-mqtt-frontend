/**
 * Created by VJE on 07.11.2017.
 */
import mqtt from 'mqtt';

/* global console */

/**
 * MqttConnection handles the connection to the MQTT broker.
 */
class MqttConnection {
  /**
     * Creates a new MqttConnection.
     *
     * @param {Object} connection - Contains config parameter to MQTT broker.
     * @param {string} connection.host - Host of MQTT broker.
     * @param {number} connection.port - Port where MQTT broker is hosted.
     * @param {string} actualStateTopic - MQTT Topic which pushes the actual
     * state of the dialyses-machine.
     * Pushed state must be one of states in the content.states array.
     */
  constructor(connection) {
    const host = connection.host;
    const options = {
      //  servers: [],
      port: connection.port,
      clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
      keepalive: 60,
      clean: false, /* false, if qos messages 1 and 2
            should be received while offline */
    };
    const clientConnection = mqtt.connect(host, options);
    clientConnection.on('connect', ()=> {
      console.info(
          'Mqtt Client is connected to broker host: '
        + host + ' port:' + options.port
      );
      clientConnection.publish('connected', 'true');
    });
    this.connection = clientConnection;
    this.mqttWildcards = this.mqttWildcards.bind(this);
  }

  /**
     * Subscribe to the given topic.
     *
     * @param {string} topic - Topic, which the client subscribes.
     * @param {number} qos - QOS level 0, 1 or 2. Default 0.
     * @param {function} callback - Is called in error case.
     */
  subscribe(topic, qos, callback) {
    console.info('MQTT_CONNECTION Client ID is: '
      + this.connection.options.clientId);
    console.info('MQTT_CONNECTION Client is subscribed to ' + topic);
    this.connection.subscribe(topic, qos, (err, granted) => {
      if (err) {
        console.error(
            'MQTT_CONNECTION Error at subscribe: ',
            err, ' Granted Topic:', granted
        );
        if (callback) {
          callback();
        }
      }
    });
  }

  /**
     * Publish a message to the given topic.
     *
     * @param {string} topic - Topic, where the message should be sent.
     * @param {Object} message - The message.
     * @param {Object} options - Options for published message.
     * @param {number} options.qos - QOS level 0, 1 or 2. Default 0.
     * @param {boolean} options.retain - If set, on subscribe
     * this message will be received immediately. Default false.
     * @param {boolean} options.dup - Set this message as duplicate.
     * Default false.
     * @param {function} callback - Is called, when error occurred.
     */
  publish(topic, message, options, callback) {
    this.connection.publish(topic, message, options, (err) => {
      if (err) {
        console.error('MQTT_CONNECTION Error at publish: ', err);
        if (callback) {
          callback();
        }
      }
    });
  }

  /**
     * Set handler for a topic.
     * Handler will be called if a message to the given topic arrived.
     *
     * @param {string} subscription - Topic, which the handler should be set.
     * @param {function} callback -
     * Handler, which should be called if message arrived
     */
  messageHandler(subscription, callback) {
    this.connection.on('message', (topic, message) => {
      console.info('MQTT_CONNECTION subscription: ', subscription
        + ' | topic: ' + topic);
      message = message.toString();
      const val = message;
      let state;
      if (val === 'true') {
        state = {val: false};
      } else if (val === 'false') {
        state = {val: false};
      } else if (isNaN(val)) {
        try {
          state = JSON.parse(message);
          if (typeof state === 'object') {
            state = {val: state};
            console.info('MQTT_CONNECTION Object: ', state);
          } else if (!state || typeof state.val === 'undefined') {
            state = {val: state};
            console.info('MQTT_CONNECTION Undefined: ', state);
          }
        } catch (err) {
          state = {val: val};
        }
      } else {
        state = {val: parseFloat(val)};
        console.info('MQTT_CONNECTION Float: ', state.val);
      }
      console.info('MQTT_CONNECTION: topic/subscription',
          topic, subscription);
      const match = this.mqttWildcards(topic, subscription);
      if (match) {
        if (callback) {
          console.info('MATCH: ', match);
          callback(topic.toString(), message);
        }
      }
    });
  }

  /**
   * @param {objectItem} topic
   * @param {Object} subscription
   * @return {*}
   */
  mqttWildcards(topic, subscription) {
    return topic.match(new RegExp('^'
      + subscription.replace(/#$/, '.*').replace(/\+/g, '[^/]+') + '$'));
  }

  /**
     * Unsubscribe from the MQTT broker to the given topic.
     *
     * @param {string} myTopic - Topic, which should be unsubscribed.
     */
  unsubscribe(myTopic) {
    console.info('MQTT_CONNECTION unsubscribe to topic: ', myTopic);
    this.connection.unsubscribe(myTopic, (err) => {
      if (err) {
        console.error(
            'MQTT_CONNECTIONError at unsubcribe: ' +
          err + ' at topic ' + myTopic
        );
      }
    });
  }
}

export {MqttConnection};
