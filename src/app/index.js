/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap-grid.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery/src/jquery.js';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import 'material-design-icons/iconfont/material-icons.css';
import {MqttConnection} from '../fsm/MqttConnection';
import {App} from './App';

library.add(fab, fas);
const mqttClient = new MqttConnection(
    {
      host: 'mqtt://localhost/mqtt',
      port: 8083
    });


ReactDOM.render(<App
  mqtt={mqttClient}/>,
document.getElementById('root'));
