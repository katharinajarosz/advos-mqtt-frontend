/* eslint-disable indent */
/* eslint no-console: 0*/
/* global console */
import React from 'react';
import PropTypes from 'prop-types';
import {EventEmitter} from 'events';
import '../components/Page/Page.css';
import '../components/Displaypanel/Displaypanel.css';
import {errors} from '../components/assets/errors';
import {Main} from './main/Main';
import {Footer} from '../components/Footer/Footer';
import {MqttConnection} from '../fsm/MqttConnection';

const ALL_TOPICS = '#';
const MAIN_TOPIC = 'main/+';
const ERROR_TOPIC = 'error/+';

let errorIds = [];
const errorHistory = [];

/**
 *
 */
class App extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      page: 'home',
      alert: 'none',
      lastDesinfection: 'Standard',
      validation: '72',
      selftest: 'bestanden',
      actions: [],
      activeErrors: [],
      historyErrors: [],
      showHistory: false,
      isDanger: false,
    };
    this.showDesinfection = this.showDesinfection.bind(this);
    this.showMain = this.showMain.bind(this);
    this.setActiveErrors = this.setActiveErrors.bind(this);
    this.clearActiveErrors = this.clearActiveErrors.bind(this);
    this.handleOnToggleHistory = this.handleOnToggleHistory.bind(this);
    this.showActions = this.showActions.bind(this);
    this.setMainValues = this.setMainValues.bind(this);
  }
  /**
   *
   */
  componentDidMount() {
    this.props.mqtt.subscribe(ALL_TOPICS, 2);
    this.setActiveErrors();
    this.setMainValues();
    console.log('MAIN didMount');
  }

  /**
   *
   */
  componentWillUnmount() {
    console.log('MAIN didUnMount');
  }

  /**
   *
   */
  showDesinfection() {
    this.setState({page: 'desinfection'});
  }
  /**
   *
   */
  showMain() {
    console.log('Page: ', this.state.page);
    this.setState({page: 'home'});
  }
  /**
   *
   */
  setActiveErrors() {
    this.props.mqtt.messageHandler(ERROR_TOPIC, (topic, message) => {
      console.log('FOOTER topic: ', topic);
      const lenght = topic.split('/').length;
      const errorId = topic.split('/')[lenght-1];
      let description = '';
      const selectedError = errors
        .filter((error) => error.errorID === errorId)[0];
      console.log('FOOTER Selected Error: ', errorId);
      if (selectedError) {
        description = selectedError.errorDescription;
      } else {
        description = 'Keine Beschreibung!';
      }
      if (errorIds.indexOf(errorId) === -1) {
        const timestamp = new Date().toLocaleTimeString();
        this.state.activeErrors.push({
          errorId: errorId,
          errorDescription: description,
          selected: false,
          timestamp: timestamp
        });
        console.log('FOOTER error added: ', selectedError);
      }
      errorIds.push(errorId);
      this.setState({activeErrors: this.state.activeErrors,
        isDanger: true,
        alert: 'red'});
    });
  }
  /**
   *
   */
  clearActiveErrors() {
    if (this.state.activeErrors.length > 0) {
      this.state.activeErrors.forEach((error) => {
        error.selected = false;
        errorHistory.push(error);
      });
      errorIds = [];
      const reverseHistory = [...errorHistory].reverse();
      this.setState({
        activeErrors: [],
        historyErrors: reverseHistory,
        actions: [],
        isDanger: false,
        alert: 'none'
      });
      console.log('FOOTER History errors reverse: ', reverseHistory);
    }
  }
  /**
   *
   */
  handleOnToggleHistory() {
    this.setState((prevState) => ({showHistory: !prevState.showHistory}));
  }
  /**
   * @param {string} id
   */
  showActions(id) {
    if (!this.state.showHistory) {
      let actions = [];
      this.state.activeErrors.forEach((error) => {
        error.selected = error.errorId === id;
      });
      errors.some((error) => {
        if (error.errorID === id) {
          return actions = error.tasks;
        } else {
          actions = ['Maßnahmen nicht verfügbar!'];
        }
      });
      this.setState({actions: actions});
    }
  }
  /**
   *
   */
  setMainValues() {
    this.props.mqtt.messageHandler(MAIN_TOPIC, (topic, message) => {
      const state = JSON.parse(message);
      const values = Object.values(state);
      console.log('MAIN: yellow => state: ', state);
      console.log('MAIN: yellow => values: ', values);
      this.setState({lastDesinfection: values[0],
      validation: values[1],
      selftest: values[2]});
      // this.setState({alert: 'yellow'});
    });
  }
  /**
   *
   * @return {*}
   */
  render() {
    return (
      <React.Fragment>
      <Main
        page={this.state.page}
        alert={this.state.alert}
        showDesinfection={this.showDesinfection}
        showMain={this.showMain}
        lastDesinfection={this.state.lastDesinfection}
        validation={this.state.validation}
        selftest={this.state.selftest}
        mqtt={this.props.mqtt}
        eventEmitter={this.props.eventEmitter}
        />
        <Footer
          actions={this.state.actions}
          activeErrors={this.state.activeErrors}
          historyErrors={this.state.historyErrors}
          showHistory={this.state.showHistory}
          showActions={this.showActions}
          clearActiveErrors={this.clearActiveErrors}
          handleOnToggleHistory={this.handleOnToggleHistory}
          isDanger={this.state.isDanger}/>
      </React.Fragment>
    );
  }
}

App.propTypes = {
  mqtt: PropTypes.instanceOf(MqttConnection),
  eventEmitter: PropTypes.instanceOf(EventEmitter)
};

export {App};
