import React from 'react';
import PropTypes from 'prop-types';
import {ButtonContainer}
  from '../../components/Layout/ButtonContainer';
import {Button} from '../../components/Button/Button';
import {VideoCheckbox}
  from '../../components/VideoCheckbox/VideoCheckbox';
import {infoVideos} from '../../components/assets/infoVideos';

const checkboxList = [];
infoVideos.forEach((video) => {
  checkboxList.push(
      <VideoCheckbox
        key={video.modalId}
        videoModalTitle={video.videoTitle}
        checkboxLabel={video.checkboxLabel}
        videoSource={video.videoSource}
        videoModalId={video.modalId}
        videoLabelId={video.labelId}
        dataTarget={video.dataTarget}/>
  );
});

const VideoCheckboxList = ({
  firstList,
  secondList,
  showDisinfectionInfoChoice,
  showDisinfectionModeChoice
}) => {
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      {firstList}
      <div
        className={'row justify-content-center py-2'}>
        <ButtonContainer
          large={false}
          small={true}>
          <Button
            hepawash={true}>
            Schlauch einfädeln
          </Button>
        </ButtonContainer>
      </div>
      {secondList}
      <div
        className={'row justify-content-around py-2'}>
        <ButtonContainer
          large={false}
          small={true}>
          <Button
            hepawash={true}
            onClick={showDisinfectionInfoChoice}>
            Zurück
          </Button>
        </ButtonContainer>
        <ButtonContainer
          large={false}
          small={true}>
          <Button
            hepawash={true}
            onClick={showDisinfectionModeChoice}>
            Weiter
          </Button>
        </ButtonContainer>
      </div>
    </div>
  );
};
VideoCheckboxList.propTypes = {
  firstList: PropTypes.array,
  secondList: PropTypes.array,
  showDisinfectionInfoChoice: PropTypes.func,
  showDisinfectionModeChoice: PropTypes.func,
};

const DisinfectionInfoContainer = ({
  showDisinfectionInfoChoice,
  showDisinfectionModeChoice
}) => {
  return (
    <VideoCheckboxList
      firstList={checkboxList.slice(0, 4)}
      secondList={checkboxList.slice(4, 6)}
      showDisinfectionInfoChoice={showDisinfectionInfoChoice}
      showDisinfectionModeChoice={showDisinfectionModeChoice}/>
  );
};
DisinfectionInfoContainer.propTypes = {
  showDisinfectionInfoChoice: PropTypes.func,
  showDisinfectionModeChoice: PropTypes.func,
};

const DisinfectionInfoNaCl = ({
  showDisinfectionInfoChoice,
  showDisinfectionModeChoice
}) => {
  return (
    <VideoCheckboxList
      firstList={checkboxList.slice(6, 10)}
      secondList={checkboxList.slice(4, 6)}
      showDisinfectionInfoChoice={showDisinfectionInfoChoice}
      showDisinfectionModeChoice={showDisinfectionModeChoice}/>
  );
};
DisinfectionInfoNaCl.propTypes = {
  showDisinfectionInfoChoice: PropTypes.func,
  showDisinfectionModeChoice: PropTypes.func,
};

export {DisinfectionInfoContainer, DisinfectionInfoNaCl};
