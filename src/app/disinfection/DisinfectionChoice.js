import React from 'react';
import PropTypes from 'prop-types';
import {Label} from '../../components/Label/Label';
import {ButtonContainer}
  from '../../components/Layout/ButtonContainer';
import {Button} from '../../components/Button/Button';

const DisinfectionInfoChoice = ({
  showDisinfectionInfoContainer,
  showDisinfectionInfoNaCl
}) => {
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      <div className="row justify-content-center py-3">
        <Label
          bold={true}>
          Desinfektion muss durchgeführt werden
        </Label>
      </div>
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionInfoContainer}>
            Desinfektion mit Container
          </Button>
        </ButtonContainer>
      </div>
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionInfoNaCl}>
            Desinfektion mit NaClBeutel
          </Button>
        </ButtonContainer>
      </div>
    </div>
  );
};
DisinfectionInfoChoice.propTypes = {
  showDisinfectionInfoContainer: PropTypes.func,
  showDisinfectionInfoNaCl: PropTypes.func,
};

const DisinfectionModeChoice = ({
  showDisinfectionShort,
  showDisinfectionCitro,
  showDisinfectionStandard,
  showDisinfectionChemical
}) => {
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionShort}>
            Kurzdesinfektion
          </Button>
        </ButtonContainer>
      </div>
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionCitro}>
            Citro-Desinfektion
          </Button>
        </ButtonContainer>
      </div>
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionStandard}>
            Standard Desinfektion
          </Button>
        </ButtonContainer>
      </div>
      <div className={'row justify-content-center py-3'}>
        <ButtonContainer>
          <Button
            hepawash={true}
            onClick={showDisinfectionChemical}>
            Chemische Reinigung
          </Button>
        </ButtonContainer>
      </div>
    </div>
  );
};
DisinfectionModeChoice.propTypes = {
  showDisinfectionShort: PropTypes.func,
  showDisinfectionCitro: PropTypes.func,
  showDisinfectionStandard: PropTypes.func,
  showDisinfectionChemical: PropTypes.func,
};

export {DisinfectionInfoChoice, DisinfectionModeChoice};
