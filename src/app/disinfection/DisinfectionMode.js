import React from 'react';
import PropTypes from 'prop-types';
import {ButtonContainer}
  from '../../components/Layout/ButtonContainer';
import {Button} from '../../components/Button/Button';
import {VideoCheckbox}
  from '../../components/VideoCheckbox/VideoCheckbox';
import {desinfectionvideos}
  from '../../components/assets/desinfectionVideos';
// import iconHeiss from '../../components/assets/iconHeiss.png';
import labelHitze from '../../components/assets/labelHitze.png';

const videoList = [];

desinfectionvideos.forEach((video) => {
  videoList.push(
      <VideoCheckbox
        key={video.modalId}
        videoModalTitle={video.videoTitle}
        checkboxLabel={video.checkboxLabel}
        videoSource={video.videoSource}
        videoModalId={video.modalId}
        videoLabelId={video.labelId}
        dataTarget={video.dataTarget}/>
  );
});

const DisinfectionCitro = ({
  inProgress,
  start,
  tempValue,
  phValue,
  showDisinfectionModeChoice
}) => {
  const citroList = videoList.slice(0, 2);
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      {citroList}
      <div className="row py-5">
        <DisinfectionShort
          buttonLeftTitle={'Citro-Desinfektion starten'}
          inProgress={inProgress}
          start={start}
          tempValue={tempValue}
          phValue={phValue}
          showDisinfectionModeChoice={showDisinfectionModeChoice}/>
      </div>
    </div>
  );
};
DisinfectionCitro.propTypes = {
  inProgress: PropTypes.bool,
  start: PropTypes.func,
  tempValue: PropTypes.string,
  phValue: PropTypes.string,
  showDisinfectionModeChoice: PropTypes.func,
};

const DisinfectionStandard = ({
  inProgress,
  start,
  tempValue,
  phValue,
  showDisinfectionModeChoice
}) => {
  const firstList = videoList.slice(2, 5);
  const secondList = videoList.slice(5, 7);
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      {firstList}
      <div className="row justify-content-center py-2">
        <ButtonContainer
          small={true}
          large={false}>
          <Button
            hepawash={true}>
            Klemme öffnen
          </Button>
        </ButtonContainer>
      </div>
      {secondList}
      <DisinfectionShort
        buttonLeftTitle={'Standard Desinfektion starten'}
        inProgress={inProgress}
        start={start}
        tempValue={tempValue}
        phValue={phValue}
        showDisinfectionModeChoice={showDisinfectionModeChoice}/>
    </div>
  );
};
DisinfectionStandard.propTypes = {
  inProgress: PropTypes.bool,
  start: PropTypes.func,
  tempValue: PropTypes.string,
  phValue: PropTypes.string,
  showDisinfectionModeChoice: PropTypes.func,
};

const DisinfectionChemical = ({
  inProgress,
  start,
  tempValue,
  phValue,
  showDisinfectionModeChoice
}) => {
  const firstList = videoList.slice(7, 10);
  const secondList = videoList.slice(10, 12);
  return (
    <div className="container
          h-100 d-flex flex-column justify-content-center">
      {firstList}
      <div className="row justify-content-center py-2">
        <ButtonContainer
          small={true}
          large={false}>
          <Button
            hepawash={true}>
            Klemme öffnen
          </Button>
        </ButtonContainer>
      </div>
      {secondList}
      <DisinfectionShort
        buttonLeftTitle={'Reinigung starten'}
        inProgress={inProgress}
        start={start}
        tempValue={tempValue}
        phValue={phValue}
        showDisinfectionModeChoice={showDisinfectionModeChoice}/>
    </div>
  );
};
DisinfectionChemical.propTypes = {
  inProgress: PropTypes.bool,
  start: PropTypes.func,
  tempValue: PropTypes.string,
  phValue: PropTypes.string,
  showDisinfectionModeChoice: PropTypes.func,
};

const DisinfectionShort = ({
  image,
  caution,
  temp,
  tempValue,
  unit,
  ph,
  phValue,
  showDisinfectionModeChoice,
  inProgress,
  start,
  stop,
  buttonLeftTitle,
  buttonRightTitle,
  backButtonTitle,
  progressPanel
}) => {
  const buttonLeft = (
    <Button hepawash={true}
      disabled={inProgress}
      onClick={start}>
      {buttonLeftTitle}
    </Button>
  );
  const buttonRight = (
    <Button hepawash={true}
      disabled={!inProgress}
      onClick={stop}>
      {buttonRightTitle}
    </Button>
  );
  return (
    <React.Fragment>
      <div className=
        "container d-flex flex-column">
        <div className={'row justify-content-between py-2'}>
          <ButtonContainer>
            {progressPanel? null : buttonLeft}
          </ButtonContainer>
          <ButtonContainer>
            {buttonRight}
          </ButtonContainer>
        </div>
        <div className="row justify-content-sm-around align-items-center py-4">
          <div className="col-12 col-md-5 py-2">
            <img className="w-25 h-auto"
              src={image}
              alt={'Heiß!'}/>
            <label className="caution">
              {caution}
            </label>
          </div>
          <div className="col-12 col-md-4 py-2">
            <label className="panelName">
              {temp}
            </label>
            <div className="badge">{tempValue}</div>
            <div className="badge">°C</div>
          </div>
          <div className="col-12 col-md-3 py-2">
            <label className="panelName">
              {ph}
            </label>
            <div className="badge">{phValue}</div>
          </div>
        </div>
        <div className={'row py-2'}>
          <ButtonContainer
            large={false}
            small={true}>
            <Button hepawash={true}
              onClick={showDisinfectionModeChoice}>
              {backButtonTitle}
            </Button>
          </ButtonContainer>
        </div>
      </div>
    </React.Fragment>
  );
};
DisinfectionShort.propTypes = {
  image: PropTypes.string,
  caution: PropTypes.string,
  temp: PropTypes.string,
  ph: PropTypes.string,
  tempValue: PropTypes.string,
  phValue: PropTypes.string,
  unit: PropTypes.string,
  inProgress: PropTypes.bool,
  start: PropTypes.func,
  stop: PropTypes.func,
  buttonLeftTitle: PropTypes.string,
  buttonRightTitle: PropTypes.string,
  backButtonTitle: PropTypes.string,
  showDisinfectionModeChoice: PropTypes.func,
  progressPanel: PropTypes.bool,
};
DisinfectionShort.defaultProps = {
  progressPanel: false,
  image: labelHitze,
  caution: 'Achtung! Heiß!',
  temp: 'Temperatur:',
  ph: 'Ph:',
  backButtonTitle: 'Zurück',
  buttonRightTitle: 'Ablauf abbrechen',
  tempValue: ' ... ',
  phValue: ' ... '
};

export {DisinfectionCitro,
  DisinfectionStandard,
  DisinfectionChemical,
  DisinfectionShort};
