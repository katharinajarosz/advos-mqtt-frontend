/* eslint-disable indent */
/* eslint no-console: 0*/
/* global console */
/* global setInterval, clearInterval */
import React from 'react';
import PropTypes from 'prop-types';
import {Label} from '../../components/Label/Label';
import {CircularProgressBar}
  from '../../components/Progress/CircularProgressBar';
import {DisinfectionShort} from './DisinfectionMode';

/**
 *
 */
class DisinfectionProgress extends React.Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      duration: this.props.duration,
      restTime: this.parseDuration(this.props.duration),
    };

    this.parseDuration = this.parseDuration.bind(this);
    this.stopProgress = this.stopProgress.bind(this);
  }

  /**
   *
   */
  componentDidMount() {
    console.log('DESINFECTION IN PROGRESS: did mount');
    const myTime = new Date(this.state.duration * 60 * 1000)
      .toUTCString()
      .replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
    console.log('DESINFECTION IN PROGRESS my time: ', myTime);
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  /**
   *
   */
  componentWillUnmount() {
    console.log('DESINFECTION IN PROGRESS: will unmount');
    clearInterval(this.timerID);
  }

  /**
   *
   */
  tick() {
    const duration = this.props.duration;
    let counter = this.state.duration;
    if (counter < 1) {
      clearInterval(this.timerID);
    } else {
      counter -= 1;
      const p = ((duration - counter) / duration) * 100;
      this.setState({duration: counter});
      console.log('Percent: ', p);
      this.setState({percentage: Math.round(p)});
      this.setState({restTime: this.parseDuration(counter)});
    }
  }

  /**
   *
   */
  stopProgress() {
    clearInterval(this.timerID);
  }
  /**
   * @param {number} number
   * @return {*}
   */
  parseDuration(number) {
    let hours = Math.floor(number / 3600);
    let minutes = Math.floor((number % 3600) / 60);
    let seconds = number % 60;
    if (hours < 10) {
      hours = '0'+hours;
    }
    if (minutes < 10) {
      minutes = '0'+minutes;
    }
    if (seconds < 10) {
      seconds = '0'+seconds;
    }
    return `${hours}:${minutes}:${seconds}`;
  }

  /**
   *@return {*}
   */
  render() {
    return (
      <div className="container
          h-100 d-flex flex-column justify-content-center">
        <div className="row justify-content-center py-3">
          <Label
            name={'Desinfektion läuft noch'}
            bold={true}/>
        </div>
        <div className="row justify-content-center py-3">
          <label className="bold">
            {this.state.restTime}
          </label>
        </div>
        <div className="row justify-content-center py-3">
          <CircularProgressBar
            percentage={this.state.percentage}
            sqSize={120}
            strokeWidth={15}/>
        </div>
        <DisinfectionShort
          progressPanel={true}
          inProgress={this.props.inProgress}
          start={this.stopProgress}
          stop={this.props.stop}
          tempValue={this.props.tempValue}
          phValue={this.props.phValue}
          showDisinfectionModeChoice={this.props.showDisinfectionModeChoice}/>
      </div>
    );
  }
}
DisinfectionProgress.propTypes = {
  inProgress: PropTypes.bool,
  start: PropTypes.func,
  stop: PropTypes.func,
  tempValue: PropTypes.string,
  phValue: PropTypes.string,
  showDisinfectionModeChoice: PropTypes.func,
  percentage: PropTypes.number,
  duration: PropTypes.number,
};

export {DisinfectionProgress};
