/* eslint-disable indent */
/* eslint no-console: 0*/
/* global console */
import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Header} from '../../components/Header/Header';
import {pages} from '../../components/assets/pages';
import {DisinfectionInfoChoice, DisinfectionModeChoice}
from './DisinfectionChoice';
import {DisinfectionInfoContainer, DisinfectionInfoNaCl}
from './DisinfectionInfo';
import {DisinfectionChemical,
  DisinfectionCitro,
  DisinfectionShort,
  DisinfectionStandard}
from './DisinfectionMode';
import {DisinfectionProgress} from './DisinfectionProgress';

/**
 *
 */
class Disinfection extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      page: 'infoChoice',
      inProgress: false,
      headerTitle: pages[0].title,
      prevPage: null,
      tempValue: '0',
      phValue: '0'
    };
    this.showDisinfectionInfoChoice =
      this.showDisinfectionInfoChoice.bind(this);
    this.showDisinfectionModeChoice =
      this.showDisinfectionModeChoice.bind(this);
    this.showDisinfectionInfoContainer =
      this.showDisinfectionInfoContainer.bind(this);
    this.showDisinfectionInfoNaCl = this.showDisinfectionInfoNaCl.bind(this);
    this.showDisinfectionShort = this.showDisinfectionShort.bind(this);
    this.showDisinfectionCitro = this.showDisinfectionCitro.bind(this);
    this.showDisinfectionStandard = this.showDisinfectionStandard.bind(this);
    this.showDisinfectionChemical = this.showDisinfectionChemical.bind(this);
    this.showDisinfectionProgress = this.showDisinfectionProgress
        .bind(this);
    this.hideDisinfectionProgress = this.hideDisinfectionProgress
        .bind(this);
  }
  /**
   *
   */
  componentDidMount() {
    console.log('DESINFECTION didMount');
  }
  /**
   *
   */
  componentWillUnmount() {
    console.log('DESINFECTION willUnmount');
  }

  /**
   *
   */
  showDisinfectionInfoChoice() {
    this.setState({page: pages[0].page});
    this.setState({headerTitle: pages[0].title});
  }
  /**
   *
   */
  showDisinfectionModeChoice() {
    this.setState({page: pages[1].page});
    this.setState({headerTitle: pages[1].title});
    this.setState({inProgress: false});
  }
  /**
   *
   */
  showDisinfectionInfoContainer() {
    this.setState({page: pages[2].page});
    this.setState({headerTitle: pages[2].title});
  }
  /**
   *
   */
  showDisinfectionInfoNaCl() {
    this.setState({page: pages[3].page});
    this.setState({headerTitle: pages[3].title});
  }
  /**
   *
   */
  showDisinfectionShort() {
    this.setState({page: pages[4].page});
    this.setState({headerTitle: pages[4].title});
  }
  /**
   *
   */
  showDisinfectionCitro() {
    this.setState({page: pages[5].page});
    this.setState({headerTitle: pages[5].title});
  }
  /**
   *
   */
  showDisinfectionStandard() {
    this.setState({page: pages[6].page});
    this.setState({headerTitle: pages[6].title});
  }
  /**
   *
   */
  showDisinfectionChemical() {
    this.setState({page: pages[7].page});
    this.setState({headerTitle: pages[7].title});
  }
  /**
   *
   */
  showDisinfectionProgress() {
    this.setState((prevState) =>
      ({inProgress: !prevState.inProgress}));
    this.setState({prevPage: this.state.page});
    this.setState({page: pages[8].page});
    const progressPanel =
      pages.filter((page) => page.page === this.state.page)[0];
    this.setState({headerTitle: progressPanel.title});
    const restTime = new Date(progressPanel.duration * 60 * 1000)
        .toUTCString()
        .replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
    console.log('Rest time: ', restTime);
    this.setState({duration: progressPanel.duration * 60});
  }

  /**
   *
   */
  hideDisinfectionProgress() {
    this.setState((prevState) =>
      ({inProgress: !prevState.inProgress}));
    console.log('Prev Panel: ', this.state.prevPage);
    this.setState({page: this.state.prevPage});
  }

  /**
   *
   * @return {*}
   */
  render() {
    const disinfectionInfoChoice = (
      <DisinfectionInfoChoice
        showDisinfectionInfoContainer={this.showDisinfectionInfoContainer}
        showDisinfectionInfoNaCl={this.showDisinfectionInfoNaCl}/>
    );
    const disinfectionModeChoice = (
      <DisinfectionModeChoice
        showDisinfectionShort={this.showDisinfectionShort}
        showDisinfectionCitro={this.showDisinfectionCitro}
        showDisinfectionStandard={this.showDisinfectionStandard}
        showDisinfectionChemical={this.showDisinfectionChemical}/>
    );
    const disinfectionInfoContainer = (
      <DisinfectionInfoContainer
        showDisinfectionInfoChoice={this.showDisinfectionInfoChoice}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionInfoNaCl = (
      <DisinfectionInfoNaCl
        showDisinfectionInfoChoice={this.showDisinfectionInfoChoice}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionShort = (
      <DisinfectionShort
        buttonLeftTitle={'Kurzdesinfektion starten'}
        inProgress={this.state.inProgress}
        start={this.showDisinfectionProgress}
        stop={this.hideDisinfectionProgress}
        tempValue={this.state.tempValue}
        phValue={this.state.phValue}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionCitro = (
      <DisinfectionCitro
        inProgress={this.state.inProgress}
        start={this.showDisinfectionProgress}
        tempValue={this.state.tempValue}
        phValue={this.state.phValue}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionStandard = (
      <DisinfectionStandard
        inProgress={this.state.inProgress}
        start={this.showDisinfectionProgress}
        tempValue={this.state.tempValue}
        phValue={this.state.phValue}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionChemical = (
      <DisinfectionChemical
        inProgress={this.state.inProgress}
        start={this.showDisinfectionProgress}
        tempValue={this.state.tempValue}
        phValue={this.state.phValue}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}/>
    );
    const disinfectionProgress = (
      <DisinfectionProgress
        inProgress={this.state.inProgress}
        start={this.showDisinfectionProgress}
        stop={this.hideDisinfectionProgress}
        tempValue={this.state.tempValue}
        phValue={this.state.phValue}
        showDisinfectionModeChoice={this.showDisinfectionModeChoice}
        percentage={this.state.percentage}
        duration={this.state.duration}
      />
    );
    return (
      <React.Fragment>
        <Header
          headerIcon={
            <FontAwesomeIcon icon="angle-double-up" size="lg"/>}
          headerTitle={this.state.headerTitle}
          onClick={this.props.showMain}
        />
        <div className='page wrapper'>
          {{infoChoice: disinfectionInfoChoice,
            modeChoice: disinfectionModeChoice,
            infoContainer: disinfectionInfoContainer,
            infoNaCl: disinfectionInfoNaCl,
            short: disinfectionShort,
            citro: disinfectionCitro,
            standard: disinfectionStandard,
            chemical: disinfectionChemical,
            progress: disinfectionProgress
          }[this.state.page]}
        </div>
      </React.Fragment>
    );
  }
}

Disinfection.propTypes = {
  showMain: PropTypes.func,
};

export {Disinfection};
