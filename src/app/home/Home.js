import React from 'react';
import PropTypes from 'prop-types';
import {ButtonContainer} from '../../components/Layout/ButtonContainer';
import {Button} from '../../components/Button/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Header} from '../../components/Header/Header';

const Home = ({
  handleDesinfection,
  lastDesinfection,
  validation,
  selftest,
  alert
}) => {
  return (
    <React.Fragment>
      <Header
        headerIcon={
          <FontAwesomeIcon icon="power-off" size="lg"/>}
        headerTitle={'Home'}
      />
      <div className={{none: 'page wrapper',
        red: 'page wrapper blink red',
        yellow: 'page wrapper blink yellow'}[alert]}>
        <div className="container
          h-100 d-flex flex-column justify-content-center">
          <div className="row justify-content-center py-2">
            <ButtonContainer>
              <Button
                hepawash={true}>
                Vorbereitung
              </Button>
            </ButtonContainer>
            <ButtonContainer>
              <Button
                hepawash={true}
                disabled={true}>
                Nachbereitung
              </Button>
            </ButtonContainer>
          </div>
          <div className="row justify-content-center py-2">
            <ButtonContainer>
              <Button
                hepawash={true}
                onClick={handleDesinfection}>
                Desinfektion
              </Button>
            </ButtonContainer>
            <ButtonContainer>
              <Button
                hepawash={true}
                disabled={true}>
                Selbsttest
              </Button>
            </ButtonContainer>
          </div>
          <div className="row justify-content-center py-2">
            <ButtonContainer>
              <Button
                hepawash={true}>
                Container befüllen
              </Button>
            </ButtonContainer>
            <ButtonContainer>
              <Button
                hepawash={true}>
                Einstellungen
              </Button>
            </ButtonContainer>
          </div>
          <div className="row justify-content-center py-1">
            <div className="col-sm-5">
              <div className="row py-2">
                <div className="col-sm-7">
                  <div className="panelName">
                    Letzte Desinfektion
                  </div>
                </div>
                <div className="col-sm-5">
                  <span className="badge">
                    {lastDesinfection}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-center py-1">
            <div className="col-sm-5">
              <div className="row py-2">
                <div className="col-sm-7">
                  <div className="panelName">
                    Gültigkeit
                  </div>
                </div>
                <div className="col-sm-5">
                  <span className="badge">
                    {validation}
                  </span>
                  <span className="badge">Stunden</span>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-center py-1">
            <div className="col-sm-5">
              <div className="row py-2">
                <div className="col-sm-7">
                  <div className="panelName">
                    Selbsttest
                  </div>
                </div>
                <div className="col-sm-5">
                  <span className="badge">
                    {selftest}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Home.propTypes = {
  handleDesinfection: PropTypes.func,
  lastDesinfection: PropTypes.string,
  validation: PropTypes.string,
  selftest: PropTypes.string,
  alert: PropTypes.string,
};

export {Home};
