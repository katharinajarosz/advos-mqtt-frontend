import React from 'react';
import PropTypes from 'prop-types';
import {EventEmitter} from 'events';
import {Home} from '../home/Home';
import {MqttConnection} from '../../fsm/MqttConnection';
import {Disinfection} from '../disinfection/Disinfection';

const Main = ({
  showDesinfection,
  page,
  showMain,
  alert,
  lastDesinfection,
  validation,
  selftest,
  mqtt,
  eventEmitter,
}) => {
  const home = (
    <Home
      handleDesinfection={showDesinfection}
      lastDesinfection={lastDesinfection}
      validation={validation}
      selftest={selftest}
      alert={alert}/>
  );
  const desinfection = <Disinfection
    mqtt={mqtt}
    eventEmitter={eventEmitter}
    alert={alert}
    showMain={showMain}/>;
  const preparation = null;
  const postprocess = null;
  const test = null;
  const containerFilling = null;
  const setting = null;
  return (
    <React.Fragment>
      <div className="main">
        {{home: home,
          desinfection: desinfection,
          preparation: preparation,
          postprocess: postprocess,
          test: test,
          containerFilling: containerFilling,
          setting: setting}[page]}
      </div>
    </React.Fragment>
  );
};

Main.propTypes = {
  showDesinfection: PropTypes.func,
  showMain: PropTypes.func,
  page: PropTypes.string,
  alert: PropTypes.string,
  lastDesinfection: PropTypes.string,
  validation: PropTypes.string,
  selftest: PropTypes.string,
  mqtt: PropTypes.instanceOf(MqttConnection),
  eventEmitter: PropTypes.instanceOf(EventEmitter),
};

export {Main};
