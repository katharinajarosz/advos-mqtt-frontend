import one from './one.mp4';
import two from './two.mp4';

const infoVideos = [
  {videoTitle:
      'Filtratschlauch an Filtratkonnektor anschließen',
  checkboxLabel:
      '1. Filtratschlauch an Filtratkonnektor anschließen',
  videoSource: one,
  modalId: 'modal1',
  labelId: 'modal1Label',
  dataTarget: '#modal1'
  },
  {videoTitle:
      'Desinfektionsadapter mit Permeatschlauch am Container verbinden',
  checkboxLabel:
      '2. Desinfektionsadapter mit Permeatschlauch am Container verbinden',
  videoSource: one,
  modalId: 'modal2',
  labelId: 'modal2Label',
  dataTarget: '#modal2'
  },
  {videoTitle:
      'Desinfektionsschlauch mit Desinfektionsadapter verbinden',
  checkboxLabel:
      '3. Desinfektionsschlauch mit Desinfektionsadapter verbinden',
  videoSource: one,
  modalId: 'modal3',
  labelId: 'modal3Label',
  dataTarget: '#modal3'
  },
  {videoTitle:
      'Desinfektionsschlauch in Prädilutionspumpe',
  checkboxLabel:
      '4. Desinfektionsschlauch in Prädilutionspumpe',
  videoSource: one,
  modalId: 'modal4',
  labelId: 'modal4Label',
  dataTarget: '#modal4'
  },
  {videoTitle:
      'Dreiwegehahn an HW-Konnektor anschließen',
  checkboxLabel:
      '5. Dreiwegehahn an HW-Konnektor anschließen',
  videoSource: one,
  modalId: 'modal5',
  labelId: 'modal5Label',
  dataTarget: '#modal5'
  },
  {videoTitle:
      'Desinfektionsschlauch an Dreiwegehahn anschließen',
  checkboxLabel:
      '6. Desinfektionsschlauch an Dreiwegehahn anschließen',
  videoSource: one,
  modalId: 'modal6',
  labelId: 'modal6Label',
  dataTarget: '#modal6'
  },
  {videoTitle:
      'Filtratschlauch an einen leeren Filtratkanister anschließen',
  checkboxLabel:
      '1. Filtratschlauch an einen leeren Filtratkanister anschließen',
  videoSource: two,
  modalId: 'modal7',
  labelId: 'modal7Label',
  dataTarget: '#modal7'
  },
  {videoTitle:
      'NaCl-Beutel an Infusionsständer hängen',
  checkboxLabel:
      '2. NaCl-Beutel an Infusionsständer hängen',
  videoSource: two,
  modalId: 'modal8',
  labelId: 'modal8Label',
  dataTarget: '#modal8'
  },
  {videoTitle:
      'Desinfektionsschlauch an NaCl-Beutel anschließen',
  checkboxLabel:
      '3. Desinfektionsschlauch an NaCl-Beutel anschließen',
  videoSource: two,
  modalId: 'modal9',
  labelId: 'modal9Label',
  dataTarget: '#modal9'
  },
  {videoTitle:
      'Desinfektionsschlauch in Prädilutionspumpe und Luftsensor einlegen',
  checkboxLabel:
      '4. Desinfektionsschlauch in Prädilutionspumpe und Luftsensor einlegen',
  videoSource: two,
  modalId: 'modal10',
  labelId: 'modal10Label',
  dataTarget: '#modal10'
  }
];

export {infoVideos};
