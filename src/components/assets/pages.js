const defaultTitle = 'Desinfektion';
const pages = [
  {
    page: 'infoChoice',
    title: defaultTitle
  },
  {
    page: 'modeChoice',
    title: defaultTitle
  },
  {
    page: 'infoContainer',
    title: defaultTitle
  },
  {
    page: 'infoNaCl',
    title: defaultTitle
  },
  {
    page: 'short',
    title: 'Kurzdesinfektion',
    duration: 10
  },
  {
    page: 'citro',
    title: 'Citro-Desinfektion',
    duration: 50
  },
  {
    page: 'standard',
    title: 'Standard Desinfektion',
    duration: 90
  },
  {
    page: 'chemical',
    title: 'Chemische Reinigung',
    duration: 75
  },
  {
    page: 'progress',
    title: ''
  },
];

export {pages};
