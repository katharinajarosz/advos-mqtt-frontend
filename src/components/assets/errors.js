const errors = [
  {
    errorID: 'A-1234',
    errorDescription: 'Beschreibung für A-1234',
    tasks: [
      'Maßnahme 1 für A-1234',
      'Maßnahme 2 für A-1234',
      'Maßnahme 3 für A-1234',
      'Maßnahme 4 für A-1234'
    ]
  },
  {
    errorID: 'B-1234',
    errorDescription: 'Beschreibung für B-1234',
    tasks: [
      'Maßnahme 1 für B-1234',
      'Maßnahme 2 für B-1234',
      'Maßnahme 3 für B-1234'
    ]
  },
  {
    errorID: 'C-1234',
    errorDescription: 'Beschreibung für C-1234',
    tasks: [
      'Maßnahme 1 für C-1234',
      'Maßnahme 2 für C-1234',
      'Maßnahme 3 für C-1234',
      'Maßnahme 4 für C-1234'
    ]
  },
  {
    errorID: 'D-1234',
    errorDescription: 'Beschreibung für D-1234',
    tasks: [
      'Maßnahme 1 für D-1234',
      'Maßnahme 2 für D-1234',
      'Maßnahme 3 für D-1234',
      'Maßnahme 4 für D-1234'
    ]
  },
  {
    errorID: 'E-1234',
    errorDescription: 'Beschreibung für E-1234',
    tasks: [
      'Maßnahme 1 für E-1234',
      'Maßnahme 2 für E-1234',
      'Maßnahme 3 für E-1234',
      'Maßnahme 4 für E-1234'
    ]
  },
  {
    errorID: 'F-1234',
    errorDescription: 'Beschreibung für F-1234',
    tasks: [
      'Maßnahme 1 für F-1234',
      'Maßnahme 2 für F-1234',
      'Maßnahme 3 für F-1234',
      'Maßnahme 4 für F-1234'
    ]
  },
  {
    errorID: 'G-1234',
    errorDescription: 'Beschreibung für G-1234',
    tasks: [
      'Maßnahme 1 für G-1234',
      'Maßnahme 2 für G-1234',
      'Maßnahme 3 für G-1234',
      'Maßnahme 4 für G-1234'
    ]
  },
  {
    errorID: 'H-1234',
    errorDescription: 'Beschreibung für H-1234',
    tasks: [
      'Maßnahme 1 für H-1234',
      'Maßnahme 2 für H-1234',
      'Maßnahme 3 für H-1234',
      'Maßnahme 4 für H-1234'
    ]
  },
  {
    errorID: 'I-1234',
    errorDescription: 'Beschreibung für I-1234',
    tasks: [
      'Maßnahme 1 für I-1234',
      'Maßnahme 2 für I-1234',
      'Maßnahme 3 für I-1234',
      'Maßnahme 4 für I-1234'
    ]
  },
  {
    errorID: 'J-1234',
    errorDescription: 'Beschreibung für J-1234',
    tasks: [
      'Maßnahme 1 für J-1234',
      'Maßnahme 2 für J-1234',
      'Maßnahme 3 für J-1234',
      'Maßnahme 4 für J-1234'
    ]
  },
  {
    errorID: 'K-1234',
    errorDescription: 'Beschreibung für K-1234',
    tasks: [
      'Maßnahme 1 für K-1234',
      'Maßnahme 2 für K-1234',
      'Maßnahme 3 für K-1234',
      'Maßnahme 4 für K-1234'
    ]
  },
  {
    errorID: 'L-1234',
    errorDescription: 'Beschreibung für L-1234',
    tasks: [
      'Maßnahme 1 für L-1234',
      'Maßnahme 2 für L-1234',
      'Maßnahme 3 für L-1234',
      'Maßnahme 4 für L-1234'
    ]
  }
];

export {errors};
