import two from './two.mp4';

const desinfectionvideos = [
  {videoTitle:
      'Konzentratkonnektoren an die ADVOS multi ankoppeln',
  checkboxLabel:
      '1. Konzentratkonnektoren an die ADVOS multi ankoppeln',
  videoSource: two,
  modalId: 'modal13',
  labelId: 'modal13Label',
  dataTarget: '#modal13'
  },
  {videoTitle:
      'Zitronensäure-Lösung anschließen',
  checkboxLabel:
      '2. Zitronensäure-Lösung anschließen',
  videoSource: two,
  modalId: 'modal14',
  labelId: 'modal14Label',
  dataTarget: '#modal14'
  },
  {videoTitle:
      'Konzentratkonnektoren an die ADVOS multi ankoppeln',
  checkboxLabel:
      '1. Konzentratkonnektoren an die ADVOS multi ankoppeln',
  videoSource: two,
  modalId: 'modal15',
  labelId: 'modal15Label',
  dataTarget: '#modal15'
  },
  {videoTitle:
      'SodaWash-Beutel an Infusionsständer hängen',
  checkboxLabel:
      '2. SodaWash-Beutel an Infusionsständer hängen',
  videoSource: two,
  modalId: 'modal16',
  labelId: 'modal16Label',
  dataTarget: '#modal16'
  },
  {videoTitle:
      'SodaWash-Schlauch im venösen Luftfänger und Klemme einlegen',
  checkboxLabel:
      '3. SodaWash-Schlauch im venösen Luftfänger und Klemme einlegen',
  videoSource: two,
  modalId: 'modal17',
  labelId: 'modal17Label',
  dataTarget: '#modal17'
  },
  {videoTitle:
      'SodaWash-Schlauch an Dreiwegehahn anschließen',
  checkboxLabel:
      '4. SodaWash-Schlauch an Dreiwegehahn anschließen',
  videoSource: two,
  modalId: 'modal18',
  labelId: 'modal18Label',
  dataTarget: '#modal18'
  },
  {videoTitle:
      'Zitronensäure-Lösung anschließen',
  checkboxLabel:
      '5. Zitronensäure-Lösung anschließen',
  videoSource: two,
  modalId: 'modal19',
  labelId: 'modal19Label',
  dataTarget: '#modal19'
  },
  {videoTitle:
      'Konzentratkonnektoren an die ADVOS multi ankoppeln',
  checkboxLabel:
      '1. Konzentratkonnektoren an die ADVOS multi ankoppeln',
  videoSource: two,
  modalId: 'modal20',
  labelId: 'modal20Label',
  dataTarget: '#modal20'
  },
  {videoTitle:
      'SodaWash-Beutel an Infusionsständer hängen',
  checkboxLabel:
      '2. SodaWash-Beutel an Infusionsständer hängen',
  videoSource: two,
  modalId: 'modal21',
  labelId: 'modal21Label',
  dataTarget: '#modal21'
  },
  {videoTitle:
      'SodaWash-Schlauch im venösen Luftfänger und Klemme einlegen',
  checkboxLabel:
      '3. SodaWash-Schlauch im venösen Luftfänger und Klemme einlegen',
  videoSource: two,
  modalId: 'modal22',
  labelId: 'modal22Label',
  dataTarget: '#modal22'
  },
  {videoTitle:
      'SodaWash-Schlauch an Dreiwegehahn anschließen',
  checkboxLabel:
      '4. SodaWash-Schlauch an Dreiwegehahn anschließen',
  videoSource: two,
  modalId: 'modal23',
  labelId: 'modal23Label',
  dataTarget: '#modal23'
  },
  {videoTitle:
      'Zitronensäure-Lösung und SodaWash anschließen',
  checkboxLabel:
      '5. Zitronensäure-Lösung und SodaWash anschließen',
  videoSource: two,
  modalId: 'modal24',
  labelId: 'modal24Label',
  dataTarget: '#modal24'
  },
];

export {desinfectionvideos};
