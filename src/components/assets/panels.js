const defaultTitle = 'Desinfektion';
const panels = [
  {
    panel: 'firstControl',
    title: defaultTitle
  },
  {
    panel: 'secondControl',
    title: defaultTitle
  },
  {
    panel: 'containerDesinfection',
    title: defaultTitle
  },
  {
    panel: 'bagDesinfection',
    title: defaultTitle
  },
  {
    panel: 'shortDesinfection',
    title: 'Kurzdesinfektion',
    duration: 10
  },
  {
    panel: 'citroDesinfection',
    title: 'Citro-Desinfektion',
    duration: 50
  },
  {
    panel: 'standardDesifection',
    title: 'Standard Desinfektion',
    duration: 90
  },
  {
    panel: 'chemicalCleaning',
    title: 'Chemische Reinigung',
    duration: 75
  },
  {
    panel: 'desinfectionInProgress',
    title: ''
  },
];

export {panels};
