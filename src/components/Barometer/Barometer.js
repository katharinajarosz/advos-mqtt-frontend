/**
 * Created by VJE on 12.01.2018.
 */
import React from 'react';
import PropTypes from 'prop-types';
import './Barometer.css';

const Barometer = (props) => {
  const height = (100 / (props.maxValue - props.minValue))
    *(props.value - props.minValue);

  const label = (
    <div className="barometerLabel">
      {props.label}
    </div>
  );

  const styleProgressbar = {
    height: height + '%'
  };

  const progressbar = (
    <div className="barometerProgress progress ">
      <div className="barometerProgressBar progress-bar"
        role="progressbar"
        aria-valuenow={props.value}
        aria-valuemin={props.minValue}
        aria-valuemax={props.maxValue} style={styleProgressbar}/>
    </div>
  );

  const badge = (value) => (
    <span className="badge barometerBadge">{value}</span>
  );

  const display = (
    <div className="container-fluid">
      <div className="row filling before"/>
      <div className="row">
        {badge(props.maxValue)}
      </div>
      <div className="row filling between"/>
      <div className="row">
        {badge(props.value)}
      </div>
      <div className="row filling between"/>
      <div className="row">
        {badge(props.minValue)}
      </div>
    </div>
  );

  const main = (
    <div className="container-fluid">
      <div className="col-xs-3 barometerColumn">
        {display}
      </div>
      <div className="col-xs-3 barometerColumn">
        <div className="container-fluid">
          <div className="ruler">
            <li className="item medium"/>
            <li className="item"/>
            <li className="item"/>
            <li className="item"/>
            <li className="item medium"/>
            <li className="item"/>
            <li className="item"/>
            <li className="item"/>
            <li className="item medium"/>
          </div>
        </div>
      </div>
      <div className="col-xs-6 barometerColumn">
        {progressbar}
      </div>
    </div>
  );

  return (
    <div className="barometer">
      <div className="barometerContainer">
        <div className="row">
          <div className="col-xs-12">
            {label}
          </div>
        </div>
        <div className="row">
          {main}
        </div>
      </div>
    </div>
  );
};

Barometer.defaultProps = {
  label: 'BarometerLabel',
  maxValue: '0',
  minValue: '0',
  value: '0'
};

Barometer.propTypes = {
  label: PropTypes.string,
  maxValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  minValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

export {Barometer};
