import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {ModalDialog} from '../ModalDialog/ModalDialog';
import {Button} from '../Button/Button';
import './Checkbox.css';

const VideoCheckbox = (props) => {
  const videoModalBody = (
    <div className="embed-responsive embed-responsive-4by3">
      <iframe className="embed-responsive-item"
        src={props.videoSource}
        type ="video/mp4"/>
    </div>
  );
  const videoModalButton = (
    <Button
      secondary={true}
      dataDismiss="modal">
      <FontAwesomeIcon icon="times" /> Schließen
    </Button>
  );
  const videoModal = (
    <ModalDialog
      modalId={props.videoModalId}
      labelId={props.videoLabelId}
      modalTitle={props.videoModalTitle}
      modalBody={videoModalBody}
      modalFooter={videoModalButton}
      large={true}/>
  );
  const checkbox = (
    <input type="checkbox"
      id={props.checkboxId}
      checked={props.isChecked}
      className="checkbox"
      onChange={ () => props.onChange}/>
  );
  const label = <label className="clabel">{props.checkboxLabel}</label>;
  const videoButton = (
    <Button
      dataToggle="modal"
      dataTarget={props.dataTarget}
      videoButton={true}>
      <FontAwesomeIcon icon='play-circle' size='lg'/>
    </Button>
  );
  return (
    <div className="row py-2">
      <div className="col-1 col-md-1">
        {checkbox}
      </div>
      <div className="col-11 col-md-10">
        {label}
      </div>
      <div className="col-12 col-md-1">
        {videoButton}
        {videoModal}
      </div>
    </div>

  );
};
VideoCheckbox.propTypes = {
  videoSource: PropTypes.string,
  videoModalId: PropTypes.string,
  videoLabelId: PropTypes.string,
  videoModalTitle: PropTypes.string,
  checkboxId: PropTypes.string,
  isChecked: PropTypes.bool,
  onChange: PropTypes.func,
  checkboxLabel: PropTypes.string,
  dataTarget: PropTypes.string,
};

export {VideoCheckbox};
