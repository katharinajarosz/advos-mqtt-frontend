import React from 'react';
import PropTypes from 'prop-types';

const Card = ({cardHeader, cardBody}) => {
  return (
    <div className="card">
      <h5 className="card-header">
        {cardHeader}
      </h5>
      <div className="card-body text-left">
        {cardBody}
      </div>
    </div>
  );
};
Card.propTypes = {
  cardHeader: PropTypes.string,
  cardBody: PropTypes.array,
};

export {Card};
