import React from 'react';
import './Timer.css';
/* global setInterval, clearInterval */

/**
 *
 */
class Timer extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  /**
   *
   */
  componentDidMount() {
    this.timerID = setInterval(
        () => this.tick(),
        1000
    );
  }

  /**
   *
   */
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  /**
   *
   */
  tick() {
    this.setState({
      date: new Date(),
    });
  }

  /**
   *
   * @return {*}
   */
  render() {
    return (
      <React.Fragment>
        <div className="timer">{this.state.date.toLocaleDateString()}</div>
        <div className="timer">{this.state.date.toLocaleTimeString()}</div>
      </React.Fragment>
    );
  }
}

export {Timer};
