import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';
import {Button} from '../Button/Button';

const Header = (props) => {
  return (
    <div className="headerpage">
      <div className="container-fluid">
        <div className="row">
          <div className="col-1">
            <Button
              footerbutton={true}
              onClick={props.onClick}>
              {props.headerIcon}
            </Button>
          </div>
          <div className="col-12 col-md-11">
            <h4>
              {props.headerTitle}
            </h4>
          </div>
        </div>
      </div>
    </div>
  );
};
Header.propTypes = {
  headerIcon: PropTypes.object,
  headerTitle: PropTypes.string,
  onClick: PropTypes.func,
};

export {Header};
