/* eslint-disable indent */
/* eslint no-console: 0*/

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './Button.css';

const style = classNames.bind(styles);

const Button = (props) => {
  const classNames = style({
    'btn': true,
    'buttonContainer': props.hepawash,
    'btn-block': props.block,
    'disabled': props.disabled,
    'danger': props.danger,
    'blue': props.blue,
    'green': props.green,
    'footerbutton': props.footerbutton,
    'btn-secondary': props.secondary,
    'btn-info': props.primary,
    'videoButton': props.videoButton,
  });

  return (
    <button type="button"
            className={classNames}
            data-toggle={props.dataToggle}
            data-target={props.dataTarget}
            data-dismiss={props.dataDismiss}
            aria-label={props.ariaLabel}
            onClick={!props.disabled? props.onClick: undefined}>
      {props.children}
    </button>
  );
};

Button.propTypes = {
  block: PropTypes.bool,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  danger: PropTypes.bool,
  blue: PropTypes.bool,
  green: PropTypes.bool,
  footerbutton: PropTypes.bool,
  hepawash: PropTypes.bool,
  secondary: PropTypes.bool,
  primary: PropTypes.bool,
  videoButton: PropTypes.bool,
  onClick: PropTypes.func,
  dataToggle: PropTypes.string,
  dataTarget: PropTypes.string,
  dataDismiss: PropTypes.string,
  ariaLabel: PropTypes.string
};

export {Button};
