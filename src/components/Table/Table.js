import React from 'react';
import PropTypes from 'prop-types';

const TableRow = ({className, onClick, children}) => {
  return (
    <tr onClick={onClick}
      className={className}>
      {children}
    </tr>
  );
};
TableRow.propTypes = {
  children: PropTypes.array,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

const Table = ({tableHeader, tableBody}) => {
  return (
    <div className="table-responsive">
      <table className="table table-bordered table-hover">
        <thead>
          {tableHeader}
        </thead>
        <tbody>
          {tableBody}
        </tbody>
      </table>
    </div>
  );
};

Table.propTypes = {
  tableHeader: PropTypes.object,
  tableBody: PropTypes.array,
};

export {TableRow, Table};
