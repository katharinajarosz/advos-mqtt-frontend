import React from 'react';
import PropTypes from 'prop-types';
import './Switch.css';

const Switch = (props) => {
  return (
    <div>
      <div className="row">
        <div className="col-sm-2">
          <label className="switch">{props.name}</label>
        </div>
        <div className="col-sm-1">
          <input className="toggle" type="checkbox"
            onChange={props.clickHandler}/>
        </div>
      </div>
      <div className={props.isBlur ? 'An' : 'Aus'}>
        <div className="container-fluid">
          <div className="row">
            {props.children}
          </div>
        </div>
      </div>
    </div>
  );
};

Switch.propTypes = {
  name: PropTypes.string,
  clickHandler: PropTypes.func,
  isBlur: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ])
};

Switch.defaultProps = {
  name: 'switch'
};

export default Switch;

