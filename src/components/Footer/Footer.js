/* eslint-disable indent */
/* eslint no-console: 0*/
// /* global console */
// import PropTypes from 'prop-types';
import React from 'react';
import PropTypes from 'prop-types';
import './Footer.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Timer} from '../Timer/Timer';
import {Button} from '../Button/Button';
import {ErrorDialog} from '../ModalDialog/ErrorDialog';
import {TimerDialog} from '../ModalDialog/TimerDialog';
import {BrightnessDialog} from '../ModalDialog/BrightnessDialog';

const Footer = (props) => {
  /**
   *
   * @return {*}
   */
  const errorDialog = (
    <ErrorDialog
      actions={props.actions}
      activeErrors={props.activeErrors}
      historyErrors={props.historyErrors}
      showHistory={props.showHistory}
      showActions={props.showActions}
      clearActiveErrors={props.clearActiveErrors}
      handleOnToggleHistory={props.handleOnToggleHistory}/>
  );
  const timerDialog = <TimerDialog/>;
  const brightnessDialog = <BrightnessDialog/>;
  const errorButton = (
    <Button
      footerbutton={true}
      block={true}
      danger={props.isDanger}
      dataToggle="modal"
      dataTarget="#infoModal">
      <FontAwesomeIcon icon="exclamation" size="lg"/>
    </Button>
  );
  const alarmButton = (
    <Button
      footerbutton={true}
      block={true}>
      <FontAwesomeIcon icon="bell" size="lg"/>
    </Button>
  );
  const brightnessButton = (
    <Button
      footerbutton={true}
      block={true}
      dataToggle="modal"
      dataTarget="#brightnessModal">
      <FontAwesomeIcon icon="sun" size="lg"/>
    </Button>
  );
  const unknownButton = (
    <Button
      footerbutton={true}
      block={true}>
      <FontAwesomeIcon icon="undo" size="lg"/>
    </Button>
  );
  const timerButton = (
    <Button
      footerbutton={true}
      block={true}
      dataToggle="modal"
      dataTarget="#timerModal">
      <FontAwesomeIcon icon="hourglass-half" size="lg"/>
    </Button>
  );
  const datetimeLabel = (
    <Timer/>
  );
  return (
    <footer className="footerpage">
      <div className="container-fluid">
        {errorDialog}
        {timerDialog}
        {brightnessDialog}
        <div className="row">
          <div className="col-12 col-md-3">
            <div className="btn-group d-flex" role="group">
              <div className="w-100">{errorButton}</div>
              <div className="w-100">{alarmButton}</div>
              <div className="w-100">{brightnessButton}</div>
            </div>
          </div>
          <div className="col-md-6"/>
          <div className="col-12 col-md-3">
            <div className="btn-group d-flex" role="group">
              <div className="w-100">{datetimeLabel}</div>
              <div className="w-100">{timerButton}</div>
              <div className="w-100">{unknownButton}</div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

Footer.propTypes = {
  activeErrors: PropTypes.array,
  historyErrors: PropTypes.array,
  actions: PropTypes.array,
  showActions: PropTypes.func,
  showHistory: PropTypes.bool,
  clearActiveErrors: PropTypes.func,
  handleOnToggleHistory: PropTypes.func,
  isDanger: PropTypes.bool
};

export {Footer};
