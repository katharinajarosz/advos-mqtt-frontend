/**
 * Created by VJE on 12.01.2018.
 */
import React from 'react';
import vis from 'vis/dist/vis.js';
import 'vis/dist/vis.css';

/* global setTimeout, document */

const INTERVAL = 1000;

/**
 * Graph is a widget
 *
 * @extends React.Component
 */
class Graph extends React.Component {
  /**
   * Initialize extra functions and graph values.
   */
  constructor() {
    super();
    this.child = null;
    this.graphDataSet = null;
    this.graph = null;
    this.addPoint = this.addPoint.bind(this);
    this.renderStep = this.renderStep.bind(this);
    this.setVisibility = this.setVisibility.bind(this);
  }

  /**
   * Set the visibility of one graph.
   *
   * @param {string} graphId - The name of the part graph
   * @param {boolean} visibility - Set the graph visible or not
   */
  setVisibility(graphId, visibility) {
    this.graph.setOptions({
      groups: {
        visibility: {
          [graphId]: visibility
        }
      }
    });
  }

  /**
   * Render the graph window new after interval time.
   */
  renderStep() {
    const now = vis.moment();
    const range = this.graph.getWindow();
    const interval = range.end - range.start;
    this.graph.setWindow(now - interval, now, {animation: true});
    setTimeout(this.renderStep, INTERVAL);
  }

  /**
   * Add a new item to one graph
   *
   * @param {Number} item - point which should be added.
   * @param {string} type - graphname to which the item should be added.
   */
  addPoint(item, type) {
    //  add a new data point to the dataset
    const now = vis.moment();
    this.graphDataSet.add({
      x: now,
      y: item,
      group: type
    });
    //  remove all data points which are no longer visible
    const range = this.graph.getWindow();
    const interval = range.end - range.start;
    const oldIds = this.graphDataSet.getIds({
      filter: function(item) {
        return item.x < range.start - interval;
      }
    });
    this.graphDataSet.remove(oldIds);
  }

  /**
   * @override
   */
  render() {
    return (
      <Graph
        id="graph"
        key={this.key}
        ref={(element) => (this.refElement = element)} />
    );
  }

  /**
   *
   * @param {string} graphName
   * @param {boolean} visibility
   */
  setvisibility(graphName, visibility) {
    if (this.refElement) {
      this.refElement.setvisibility(graphName, visibility);
    }
  }

  /**
   *
   * @param {number} item
   * @param {string} graphName
   */
  addItem(item, graphName) {
    if (this.refElement) {
      this.refElement.addPoint(item, graphName);
    }
  }

  /**
   * React.componentDidMount
   */
  componentDidMount() {
    this.child = document.getElementById('graph');

    this.graphDataSet = new vis.DataSet();
    const options = {
      start: vis.moment().add(-30, 'seconds'),
      end: vis.moment(),
      drawPoints: {
        style: 'circle'
      },
      shaded: {
        orientation: 'bottom' // top, bottom
      }
    };
    this.graph = new vis.Graph2d(this.child, this.graphDataSet, options);

    this.renderStep();
  }
}

export {Graph};
