/**
 * Created by VJE on 10.11.2017.
 */
import React from 'react';
import PropTypes from 'prop-types';
import 'jquery/src/jquery.js';
import './Displaypanel.css';

const Displaypanel = (props) => {
  const name = (
    <React.Fragment>
      <div className="panelName">{props.name}</div>
    </React.Fragment>
  );

  const content = (
    <React.Fragment>
      <div className="badge">{props.content}</div>
    </React.Fragment>
  );

  const unit = (
    <React.Fragment>
      <div className="badge">{props.unit}</div>
    </React.Fragment>
  );

  return (
    <div className="row py-2">
      <div className="col-sm-7">
        {name}
      </div>
      <div className="col-sm-5">
        {content}
        {unit}
      </div>
    </div>
  );
};

Displaypanel.propTypes = {
  name: PropTypes.string,
  content: PropTypes.string,
  unit: PropTypes.string
};

export default Displaypanel;
