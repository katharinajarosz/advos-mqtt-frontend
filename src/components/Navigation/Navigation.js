/**
 * Created by VJE on 03.01.2018.
 */
import React from 'react';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import 'jquery/src/jquery.js';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/js/collapse.js';
import './Navigation.css';

const NavBar = (props) => {
  return (
    <nav className="navBar navbar navbar-default">
      <div className="navBarContainer container-fluid">

        <div className="navbar-header">
          <button
            type="button"
            className="navBarToggle navbar-toggle"
            data-toggle="collapse"
            data-target="#myNavbar">
            <span className="icon-bar"/>
            <span className="icon-bar"/>
            <span className="icon-bar"/>
          </button>

        </div>

        <div className="collapse navbar-collapse" id="myNavbar">
          <ul className="myNav nav navbar-nav">
            {props.children}
          </ul>
        </div>
      </div>
    </nav>
  );
};

NavBar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ])
};

const NavItem = (props) => {
  return (
    <li role="presentation" className="navBarItem">
      <NavLink
        exact
        strict
        data-toggle="collapse"
        data-target=".navbar-collapse.in"
        to={props.to}>
        {props.name}
      </NavLink>
    </li>
  );
};

NavItem.propTypes = {
  name: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired
};

export {NavBar, NavItem};
