/**
 * Created by VJE on 10.01.2018.
 */
import React from 'react';
import PropTypes from 'prop-types';
import './Input.css';

const Input = (props) => {
  return (
    <div className="input">
      <div className="container">
        <div className="row">
          <label className="inputLabel col-xs-1">
            {props.name}
          </label>

          <div className="inputContainer
                             input-group
                             input-group-lg
                             col-xs-2">
            <input onChange={props.onChange}
              onBlur={props.onBlur}
              type="text"
              value={props.value}
              className="inputValue form-control"
              aria-describedby="basic-addon2"/>
            <span className="inputUnit input-group-addon"
              id="">
              {props.unit}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

Input.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  unit: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func
};

export {Input};
