/**
 * created by PBV on 28.08.2018
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './Label.css';

const style = classNames.bind(styles);

const Label = (props) => {
  const classNames = style({
    'bold': props.bold,
    'normal': props.normal,
  });
  return (
    <div className="row">
      <p className={classNames}>
        {props.children}
      </p>
    </div>
  );
};

Label.propTypes = {
  children: PropTypes.any,
  bold: PropTypes.bool,
  normal: PropTypes.bool,
};

export {Label};
