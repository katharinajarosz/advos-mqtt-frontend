import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames/bind';
import styles from '../Button/Button.css';

const style = classNames.bind(styles);

const ButtonContainer = ({large, small, children}) => {
  const classNames = style({
    'col-12 col-md-4 py-1': large,
    'col-12 col-md-3 py-2': small,
  });
  return (
    <div className={classNames}>
      {children}
    </div>
  );
};

ButtonContainer.propTypes = {
  children: PropTypes.any,
  large: PropTypes.bool,
  small: PropTypes.bool,
};

ButtonContainer.defaultProps = {
  large: true
};

export {ButtonContainer};
