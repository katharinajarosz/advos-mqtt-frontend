import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from 'bootstrap/dist/css/bootstrap.css';

const style = classNames.bind(styles);

const ModalDialog = ({
  modalId,
  labelId,
  modalTitle,
  modalBody,
  modalFooter,
  scrollable,
  large}) => {
  const classNames = style({
    'modal-dialog': true,
    'modal-dialog-centered': true,
    'modal-dialog-scrollable': scrollable,
    'modal-lg': large,
  });
  return (
    <div
      className="modal fade"
      id={modalId}
      tabIndex="-1"
      role="dialog"
      aria-labelledby={labelId}>
      <div
        className={classNames}
        role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title"
              id={labelId}>
              {modalTitle}
            </h4>
          </div>
          <div className="modal-body">
            {modalBody}
          </div>
          <div className="modal-footer">
            {modalFooter}
          </div>
        </div>
      </div>
    </div>
  );
};
ModalDialog.propTypes = {
  modalId: PropTypes.string,
  labelId: PropTypes.string,
  modalTitle: PropTypes.string,
  modalBody: PropTypes.object,
  modalFooter: PropTypes.object,
  scrollable: PropTypes.bool,
  large: PropTypes.bool
};

export {ModalDialog};
