import React from 'react';
import {Button} from '../Button/Button';
import {ModalDialog} from './ModalDialog';
import PropTypes from 'prop-types';

const BrightnessDialog = (props) => {
  const btn = (
    <Button
      secondary={true}
      dataDismiss="modal">
      Schließen
    </Button>
  );
  const body = null;
  return (
    <ModalDialog
      modalId={'brightnessModal'}
      labelId={'brightnessrModalLabel'}
      modalTitle={'Helligkeit'}
      modalBody={body}
      modalFooter={btn}/>
  );
};
BrightnessDialog.propTypes = {
  modalId: PropTypes.string,
  labelId: PropTypes.string,
  modalTitle: PropTypes.string,
  modalBody: PropTypes.object,
  modalFooter: PropTypes.object,
};

export {BrightnessDialog};
