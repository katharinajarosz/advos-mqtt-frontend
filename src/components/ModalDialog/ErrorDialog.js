import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Button} from '../Button/Button';
import {TableRow, Table} from '../Table/Table';
import {Card} from '../Card/Card';
import {ModalDialog} from './ModalDialog';

const ErrorDialog = (props) => {
  // TODO comment
  const errorHeader = (
    <TableRow>
      <th>Fehler</th>
      <th>Beschreibung</th>
    </TableRow>
  );
  const errorRows = [];
  props.activeErrors.forEach((activeError) => {
    errorRows.push(
        <TableRow
          onClick={() => props.showActions(activeError.errorId)}
          className={activeError.selected? 'bg-warning' : ''}
          key={activeError.errorId}>
          <td>{activeError.errorId}</td>
          <td>{activeError.errorDescription}</td>
        </TableRow>
    );
  });
  // TODO comment
  const historyHeader = (
    <TableRow className="table-info">
      <th>Fehler</th>
      <th>Zeitmarke</th>
      <th>Beschreibung</th>
    </TableRow>
  );
  const historyRows = [];
  props.historyErrors.forEach((historyError) => {
    historyRows.push(
        <TableRow
          key={historyError.timestamp}>
          <td>{historyError.errorId}</td>
          <td>{historyError.timestamp}</td>
          <td>{historyError.errorDescription}</td>
        </TableRow>
    );
  });
  // TODO comment
  const actionsList = [];
  props.actions.forEach((action) => {
    actionsList.push(
        <ul key={action} className="list-group list-group-flush">
          <li className="list-group-item">{action}</li>
        </ul>
    );
  });
  const actionsCard = (
    <Card
      cardHeader={'Maßnahmen zur Fehlerbehebung'}
      cardBody={actionsList}/>
  );
  const errorTable = (
    <Table
      tableHeader={errorHeader}
      tableBody={errorRows}/>
  );
  const historyTable = (
    <Table
      tableHeader={historyHeader}
      tableBody={historyRows}/>
  );
  const modalTitle = (props.showHistory?
      'Fehlerhistorie' :
      'Aktive Fehler'
  );
  const modalBody = (
    <div className="container d-flex flex-column">
      <div className="row align-items-center">
        <div className="col">
          {props.showHistory? null: actionsCard}
        </div>
      </div>
      <div className="row pt-3">
        <div className="col">
          {props.showHistory? historyTable: errorTable}
        </div>
      </div>
    </div>
  );
  const modalFooter = (
    <div
      className="container
            d-flex flex-column justify-content-center">
      <div className="row justify-content-center justify-content-md-end">
        <div className="p-1">
          {props.showHistory? null:
            <Button
              secondary={true}
              onClick={props.clearActiveErrors}>
              <FontAwesomeIcon icon="undo" /> Fehler zurücksetzen
            </Button>}
        </div>
        <div className="p-1">
          <Button
            secondary={!props.showHistory}
            primary={props.showHistory}
            onClick={props.handleOnToggleHistory}>
            <FontAwesomeIcon icon="history" />
            {props.showHistory?
              ' Fehlerhistorie aus' :
              ' Zur Fehlerhistorie'}
          </Button>
        </div>
        <div className="p-1">
          <Button
            secondary={true}
            dataDismiss="modal">
            <FontAwesomeIcon icon="times" /> Schließen
          </Button>
        </div>
      </div>
    </div>
  );
  return (
    <ModalDialog
      modalId={'infoModal'}
      labelId={'infoModalLabel'}
      modalTitle={modalTitle}
      modalBody={modalBody}
      modalFooter={modalFooter}
      scrollable={true}
      large={true}/>
  );
};

ErrorDialog.propTypes = {
  activeErrors: PropTypes.array,
  historyErrors: PropTypes.array,
  actions: PropTypes.array,
  showActions: PropTypes.func,
  showHistory: PropTypes.bool,
  clearActiveErrors: PropTypes.func,
  handleOnToggleHistory: PropTypes.func
};

export {ErrorDialog};
