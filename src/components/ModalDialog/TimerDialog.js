import React from 'react';
// import PropTypes from 'prop-types';

import {ModalDialog} from './ModalDialog';
import {Button} from '../Button/Button';
import PropTypes from 'prop-types';

const TimerDialog = (props) => {
  const btn = (
    <Button
      secondary={true}
      dataDismiss="modal">
      Schließen
    </Button>
  );
  const body = null;
  return (
    <ModalDialog
      modalId={'timerModal'}
      labelId={'timerModalLabel'}
      modalTitle={'Timer'}
      modalBody={body}
      modalFooter={btn}/>
  );
};
TimerDialog.propTypes = {
  modalId: PropTypes.string,
  labelId: PropTypes.string,
  modalTitle: PropTypes.string,
  modalBody: PropTypes.object,
  modalFooter: PropTypes.object,
};

export {TimerDialog};
